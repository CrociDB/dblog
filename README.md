# dblog

A very simple and minimalistic blog system written in Python and Flask, using the MongoDB database.

## Features

All the above features are planned, since it's in a very early stage of development:

 - localization support
 - multi-language posts support
 - markdown post editor
 - code syntax highlighting on posts
 - easy embeded video adding
 - clean and lightweight theme

## Instructions

After cloning the repository, create a virtualenv and install necessary libraries:

	$ sudo pip install virtualenv
    $ virtualenv dblogenv
    $ source dblogenv/bin/activate
    $ pip install -r requirements.txt

Then run the project with:

    $ python dblog.py

## Deploy

I'll write the instructions to deploy later.
